<html>
<head>
	<meta name="decorator" content="adminpage">
</head>
<body>
	[@ui.header pageKey="cloudfoundry.admin.target.heading" /]
	<div class="aui-message error">
	    <p class="title">
	        <span class="aui-icon icon-error"></span>
	        <strong>[@ww.text name='cloudfoundry.admin.target.delete.error.message.header' /]</strong>
	    </p>
	    <p>[@ww.text name='cloudfoundry.admin.target.delete.error.message.description' /]</p>
	</div>
	[@ui.clear/]
	
	<p>[@ww.text name='cloudfoundry.admin.target.delete.error.details' /] <em>${name}</em>:</p>

	<table id="jobs" class="aui" width="100%">
		<thead><tr>
			<th class="labelPrefixCell">[@ww.text name='cloudfoundry.global.targets.list.heading.target' /]</th>
			<th class="operations">[@ww.text name='cloudfoundry.global.targets.list.heading.operations' /]</th>
		</tr></thead>
		<tbody>	
			[#foreach job in jobsUsingTarget]
				<tr>
					<td>
						${job.name}<br />
						[#if job.description?has_content]
							<span class="subGrey">${job.description}</span>
						[/#if]
					</td>
					<td>
						<a href="${req.contextPath}/build/admin/edit/editBuildTasks.action?buildKey=${job.key}">[@ww.text name='cloudfoundry.global.edit.job' /]</a>
					</td>
				</tr>			
			[/#foreach]
		</tbody>
	</table>
</body>
</html>