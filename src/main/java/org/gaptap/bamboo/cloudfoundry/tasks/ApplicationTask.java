/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.google.common.collect.Lists;
import org.cloudfoundry.operations.applications.ApplicationSummary;
import org.gaptap.bamboo.cloudfoundry.cli.CliFormatter;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.APP_NAME_SEARCH_REGEX;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.APP_NAME_SEARCH_VARIABLE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.DELETE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.MAP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.MAP_URI;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_APP_NAME_SEARCH;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_DELETE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_LIST;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_MAP;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_RENAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_RESTART;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_SHOW;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_START;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_STOP;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_UNMAP;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.RENAME_FAIL_IF_NOT_EXISTS;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.RENAME_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.RENAME_NEWNAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.RESTART_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.SELECTED_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.SHOW_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.START_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.STOP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.UNMAP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.UNMAP_URI;

/**
 * @author David Ehringer
 * 
 */
public class ApplicationTask extends AbstractCloudFoundryTask {

    public ApplicationTask(EncryptionService encryptionService) {
        super(encryptionService);
    }

    @Override
    @NotNull
    public TaskResult doExecute(@NotNull CommonTaskContext taskContext) throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext).success();

        ConfigurationMap configMap = taskContext.getConfigurationMap();
        String option = configMap.get(SELECTED_OPTION);

        CloudFoundryService cloudFoundry = getCloudFoundryService(taskContext);

        try {
            if (OPTION_LIST.equals(option)) {
                buildLogger.addBuildLogEntry("Getting apps " + getLoginContext(taskContext));
                list(cloudFoundry, buildLogger, taskResultBuilder);
            } else if (OPTION_SHOW.equals(option)) {
                buildLogger.addBuildLogEntry("Showing health and status for app " + configMap.get(SHOW_NAME) + " " + getLoginContext(taskContext));
                show(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else if (OPTION_START.equals(option)) {
                buildLogger.addBuildLogEntry("Starting app " + configMap.get(START_NAME) + " " + getLoginContext(taskContext));
                start(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else if (OPTION_STOP.equals(option)) {
                buildLogger.addBuildLogEntry("Stopping app " + configMap.get(STOP_NAME) + " " + getLoginContext(taskContext));
                stop(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else if (OPTION_RESTART.equals(option)) {
                buildLogger.addBuildLogEntry("Restarting app " + configMap.get(RESTART_NAME) + " " + getLoginContext(taskContext));
                restart(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else if (OPTION_DELETE.equals(option)) {
                buildLogger.addBuildLogEntry("Deleting app " + configMap.get(DELETE_NAME) + " " + getLoginContext(taskContext));
                delete(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else if (OPTION_MAP.equals(option)) {
                buildLogger.addBuildLogEntry("Adding route " + configMap.get(MAP_URI) + " to app " + configMap.get(MAP_NAME) + " " + getLoginContext(taskContext));
                map(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else if (OPTION_UNMAP.equals(option)) {
                buildLogger.addBuildLogEntry("Removing route " + configMap.get(UNMAP_URI) + " from app " + configMap.get(UNMAP_NAME) + " " + getLoginContext(taskContext));
                unmap(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else if (OPTION_RENAME.equals(option)) {
                buildLogger.addBuildLogEntry("Renaming app " + configMap.get(RENAME_NAME) + " to " + configMap.get(RENAME_NEWNAME) + " " + getLoginContext(taskContext));
                rename(cloudFoundry, configMap, taskResultBuilder, buildLogger);
            } else if (OPTION_APP_NAME_SEARCH.equals(option)) {
                buildLogger.addBuildLogEntry("Task: Searching for application name.");
                appNameSearch(cloudFoundry, taskContext, taskResultBuilder);
            } else {
                throw new TaskException("Unknown or unspecified application management option: " + option);
            }
        } catch (InterruptedException e) {
            buildLogger.addErrorLogEntry("Unable to complete task due to unknown error: " + e.getMessage());
            taskResultBuilder.failedWithError();
        }

        return taskResultBuilder.build();
    }

    private void list(CloudFoundryService cloudFoundry, BuildLogger buildLogger, TaskResultBuilder taskResultBuilder)
            throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        CliFormatter cliFormatter = new CliFormatter(buildLogger);
        cloudFoundry.apps()
                .collectList()
                .subscribe(applicationSummaries -> {
                            cliFormatter.cfApps(applicationSummaries);
                        },
                        throwable -> {
                            buildLogger.addErrorLogEntry("Unable to show apps: " + throwable.getMessage());
                                    taskResultBuilder.failedWithError();
                            latch.countDown();
                        },
                        latch::countDown);

        latch.await();
    }

    private void show(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap,
                      TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String name = configMap.get(SHOW_NAME);
        doShow(cloudFoundry, buildLogger, taskResultBuilder, name);
    }

    private void doShow(CloudFoundryService cloudFoundry, BuildLogger buildLogger, TaskResultBuilder taskResultBuilder, String name) throws InterruptedException {
        CliFormatter cliFormatter = new CliFormatter(buildLogger);
        CountDownLatch latch = new CountDownLatch(1);
        cloudFoundry.app(name)
            .subscribe(applicationDetail -> {
                        cliFormatter.cfApp(applicationDetail);
                    },
                    throwable -> {
                        buildLogger.addErrorLogEntry("Unable to show app: " + throwable.getMessage());
                                taskResultBuilder.failedWithError();
                        latch.countDown();
                    },
                    latch::countDown);

        latch.await();
    }

    private void start(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap,
                       TaskResultBuilder taskResultBuilder) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        String name = configMap.get(START_NAME);
        doSubscribe(cloudFoundry.startApp(name), "Unable to start app.", buildLogger, taskResultBuilder);
        doShow(cloudFoundry, buildLogger, taskResultBuilder, name);
    }

    private void stop(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap,
                      TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String name = configMap.get(STOP_NAME);
        doSubscribe(cloudFoundry.stopApp(name), "Unable to stop app.", buildLogger, taskResultBuilder);
    }

    private void restart(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap,
                         TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String name = configMap.get(RESTART_NAME);
        doSubscribe(cloudFoundry.restartApp(name), "Unable to restart app.", buildLogger, taskResultBuilder);
        doShow(cloudFoundry, buildLogger, taskResultBuilder, name);
    }

    private void delete(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap,
                        TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String name = configMap.get(DELETE_NAME);
        doSubscribe(cloudFoundry.deleteApp(name), "Unable to delete app.", buildLogger, taskResultBuilder);
    }

    private void map(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap,
                     TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String name = configMap.get(MAP_NAME);
        String uri = configMap.get(MAP_URI);
        doSubscribe(cloudFoundry.map(name, uri), "Unable to map route to app.", buildLogger, taskResultBuilder);
    }

    private void unmap(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap,
                       TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String name = configMap.get(UNMAP_NAME);
        String uri = configMap.get(UNMAP_URI);
        doSubscribe(cloudFoundry.unmap(name, uri), "Unable to unmap route to app.", buildLogger, taskResultBuilder);
    }

    private void rename(CloudFoundryService cloudFoundry, ConfigurationMap configMap,
            TaskResultBuilder taskResultBuilder, BuildLogger buildLogger) throws InterruptedException {
        String name = configMap.get(RENAME_NAME);
        String newName = configMap.get(RENAME_NEWNAME);
        String failIfNotExists = configMap.get(RENAME_FAIL_IF_NOT_EXISTS);
        doSubscribe(cloudFoundry.renameApp(name, newName, Boolean.valueOf(failIfNotExists)), "Unable to rename app.", buildLogger, taskResultBuilder);
    }

    private void appNameSearch(CloudFoundryService cloudFoundry, CommonTaskContext taskContext,
            TaskResultBuilder taskResultBuilder) throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        ConfigurationMap configMap = taskContext.getConfigurationMap();
        String regex = configMap.get(APP_NAME_SEARCH_REGEX);
        String variable = configMap.get(APP_NAME_SEARCH_VARIABLE);

        List<String> appNames = Lists.newArrayList();
        Pattern pattern = Pattern.compile(regex);
        for (ApplicationSummary app : cloudFoundry.apps().collectList().block()) {
            Matcher matcher = pattern.matcher(app.getName());
            if (matcher.matches()) {
                appNames.add(app.getName());
            }
        }

        if (appNames.size() == 0) {
            buildLogger.addErrorLogEntry("No application found matching regex " + regex);
            taskResultBuilder.failed();
        } else if (appNames.size() > 1) {
            StringBuilder message = new StringBuilder();
            message.append(appNames.size());
            message.append(" applications found matching regex ");
            message.append(regex);
            message.append(". Only one match is allowed. Matching applications: ");
            for (String app : appNames) {
                message.append(app);
                message.append(" ");
            }
            buildLogger.addErrorLogEntry(message.toString());
            taskResultBuilder.failed();
        } else {
            if (taskContext instanceof TaskContext) {
                Map<String, String> customBuildData = ((TaskContext) taskContext).getBuildContext().getBuildResult()
                        .getCustomBuildData();
                customBuildData.put(variable, appNames.get(0));
            } else if (taskContext instanceof DeploymentTaskContext) {
                Map<String, String> customBuildData = ((DeploymentTaskContext) taskContext).getDeploymentContext()
                        .getCurrentResult().getCustomBuildData();
                customBuildData.put(variable, appNames.get(0));
            }
        }
    }
}
