/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.admin;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.Table;

/**
 * @author David Ehringer
 */
@Preload
@Table("Target")
public interface Target extends Entity, Nameable, Describable {

	// @NotNull
	String getUrl();

	void setUrl(String url);
	
	Credentials getCredentials();
	
	void setCredentials(Credentials credentials);

	Proxy getProxy();
	
	void setProxy(Proxy proxy);
	
	boolean isTrustSelfSignedCerts();
	
	void setTrustSelfSignedCerts(boolean trustSelfSignedCerts);
	
	boolean isDisableForBuildPlans();
	
	void setDisableForBuildPlans(boolean disableForBuildPlans);
}
