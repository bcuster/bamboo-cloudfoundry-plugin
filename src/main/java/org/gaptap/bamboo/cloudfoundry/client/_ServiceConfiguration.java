package org.gaptap.bamboo.cloudfoundry.client;

import org.gaptap.bamboo.cloudfoundry.Nullable;
import org.immutables.value.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Value.Immutable
abstract class _ServiceConfiguration {

    abstract String getService();

    abstract String getServiceInstance();

    abstract String getPlan();

    @Nullable
    abstract  Map<String, Object> getParameters();

    @Nullable
    abstract List<String> getTags();
}
