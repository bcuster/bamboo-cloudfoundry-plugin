/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

/**
 * @author David Ehringer
 * 
 */
public class Log4jLogger implements Logger {

	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger("CloudFoundryClient");

	@Override
	public void info(String message) {
		LOG.info(message);
	}

	@Override
	public void warn(String message) {
		LOG.warn(message);
	}

	@Override
	public void error(String message) {
		LOG.error(message);
	}

}
