package org.gaptap.bamboo.cloudfoundry.tasks;

import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskState;
import org.junit.Before;
import org.junit.Test;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class RoutingTaskTest extends AbstractTaskTest{

    private RoutingTask task;

    @Before
    public void init(){
        createDefaultConfigMap();

        task = new RoutingTask(encryptionService);
        task.setCloudFoundryServiceFactory(cloudFoundryServiceFactory);
    }

    private void createDefaultConfigMap() {
        configurationMap = new ConfigurationMapImpl();
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);
    }

    @Test
    public void bindRouteServiceWithMinimumOptions() throws TaskException {
        // Given
        configurationMap.put(SELECTED_OPTION, OPTION_BIND_ROUTE_SERVICE);
        configurationMap.put(SERVICE_NAME_BIND_SERVICE, "my-test-service");
        configurationMap.put(DOMAIN_BIND_SERVICE, "cloudfoundry.org");
        configurationMap.put(SELECTED_DATA_OPTION, DATA_OPTION_INLINE);

        when(cloudFoundryService.bindRouteService(anyString(), anyString(), anyString(), anyString(), anyMap())).thenReturn(Mono.empty());

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        verify(cloudFoundryService).bindRouteService("my-test-service", "cloudfoundry.org", null, null, new HashMap<>());
    }

    @Test
    public void bindRouteServiceWithAllPrimaryOptions() throws TaskException {
        // Given
        configurationMap.put(SELECTED_OPTION, OPTION_BIND_ROUTE_SERVICE);
        configurationMap.put(SERVICE_NAME_BIND_SERVICE, "my-test-service");
        configurationMap.put(DOMAIN_BIND_SERVICE, "cloudfoundry.org");
        configurationMap.put(HOST_BIND_SERVICE, "docs");
        configurationMap.put(PATH_BIND_SERVICE, "/current");
        configurationMap.put(SELECTED_DATA_OPTION, DATA_OPTION_INLINE);

        when(cloudFoundryService.bindRouteService(anyString(), anyString(), anyString(), anyString(), anyMap())).thenReturn(Mono.empty());

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        verify(cloudFoundryService).bindRouteService("my-test-service", "cloudfoundry.org", "docs", "/current", new HashMap<>());
    }

    @Test
    public void bindRouteServiceWithInlineJsonParameters() throws TaskException {
        // Given
        configurationMap.put(SELECTED_OPTION, OPTION_BIND_ROUTE_SERVICE);
        configurationMap.put(SERVICE_NAME_BIND_SERVICE, "my-test-service");
        configurationMap.put(DOMAIN_BIND_SERVICE, "cloudfoundry.org");
        configurationMap.put(SELECTED_DATA_OPTION, DATA_OPTION_INLINE);
        configurationMap.put(JSON_PARAMS_INLINE, "{\"some\": \"value\", \"more\": \"stuff\"}");

        when(cloudFoundryService.bindRouteService(anyString(), anyString(), anyString(), anyString(), anyMap())).thenReturn(Mono.empty());

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        Map<String, Object> jsonParameters = new HashMap<>();
        jsonParameters.put("some", "value");
        jsonParameters.put("more", "stuff");
        verify(cloudFoundryService).bindRouteService("my-test-service", "cloudfoundry.org", null, null, jsonParameters);
    }

    @Test
    public void bindRouteServiceWithFileJsonParameters() throws TaskException {
        // Given
        configurationMap.put(SELECTED_OPTION, OPTION_BIND_ROUTE_SERVICE);
        configurationMap.put(SERVICE_NAME_BIND_SERVICE, "my-test-service");
        configurationMap.put(DOMAIN_BIND_SERVICE, "cloudfoundry.org");
        configurationMap.put(SELECTED_DATA_OPTION, DATA_OPTION_FILE);
        configurationMap.put(JSON_PARAMS_FILE, "src/test/resources/bind-route-service/parameters.json");

        when(cloudFoundryService.bindRouteService(anyString(), anyString(), anyString(), anyString(), anyMap())).thenReturn(Mono.empty());

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        Map<String, Object> jsonParameters = new HashMap<>();
        jsonParameters.put("first", "value");
        jsonParameters.put("second", "option");
        verify(cloudFoundryService).bindRouteService("my-test-service", "cloudfoundry.org", null, null, jsonParameters);
    }
}
